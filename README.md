# ShortMail

The project aim is to summarize the emails.
The project is an implementation of:-
 https://www.researchgate.net/publication/336248932_Text_Document_Summarization_using_Word_Embedding
 with little improvement.


#How to Run
run the following commands:-
`export PYTHONPATH=$pwd`
`python summarizer_api.py`